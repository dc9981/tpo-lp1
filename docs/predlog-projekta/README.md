# Predlog projekta

| | |
|:---|:---|
| **Naziv projekta** | **TO-DO** naziv projekta |
| **Člani projektne skupine** | **TO-DO** 1. član, 2. član, 3. član in 4. član |
| **Kraj in datum** | **TO-DO** kraj, datum |



## Povzetek projekta

**TO-DO**

* V približno 100 - 150 besedah povzemite cilje projekta in pričakovane rezultate. 
* V povzetku naj bo jasno razvidna problemska domena, s katero se boste v okviru projekta ukvarjali.



## 1. Motivacija

**TO-DO** 

* Zakaj se vam zdi smiselno, da izbrano problemsko domeno podprete s programsko rešitvijo? 
* V čem se vaša rešitev razlikuje od sorodnih rešitev, ki morebiti že obstajajo? 
* Ste v kakšnem pogledu posebni, morda edinstveni? Kakšno ciljno publiko naslavljate s svojim projektom? 
* S čim boste tipičnega predstavnika ciljne publike prepričali, da bo vaš izdelek uporabljal ali celo kupil?



## 2. Cilji projekta in pričakovani rezultati

**TO-DO**

* Rezultat vašega projekta bo programski izdelek. Na tem mestu ga opišite z uporabniškega in tehniškega vidika, vendar brez pretiranih podrobnosti. 
* Kaj bo vaš izdelek ponujal uporabniku? 
* Kako se bo uporabnik posluževal vašega izdelka? 
* Kakšna bo v grobem zgradba vašega izdelka? 
* Katere tehnologije boste uporabljali? 


### 2.1 Opis ciljev

**TO-DO**


### 2.2 Pričakovani rezultati

**TO-DO**



## 3. Projektni načrt


### 3.1 Povzetek razdelitve projekta na aktivnosti

**TO-DO**

* Na kratko povzemite razdelitev projekta na aktivnosti.
* Aktivnost je smiselno zaokrožena enota dela z jasno opredeljenimi cilji. 
* Aktivnosti so med sabo lahko odvisne: preden pričnemo izvajati neko aktivnost, morajo biti zaključene nekatere druge aktivnosti.


### 3.2 Načrt posameznih aktivnosti

**TO-DO**

Razdelite delo na projektu na aktivnosti, ki jih opišete. Za opis posamične aktivnosti lahko uporabite naslednjo tabelo:

| **Oznaka aktivnosti** | **TO-DO** kratka oznaka, npr. A1 |
| :-------------------- | :------------------------------- |
| **Predvideni datum pričetka izvajanja aktivnosti** | **TO-DO** datum |
| **Predvideni datum zaključka izvajanja aktivnosti** | **TO-DO** datum |
| **Trajanje** | **TO-DO** število dni |
| **Naziv aktivnosti** | **TO-DO** naziv (ime) aktivnosti |
| **Obseg aktivnosti v ČM** | **TO-DO** število človek-mesecev (ČM) |
| **Seznam ciljev aktivnosti (kaj želite doseči)** | **TO-DO** navedite cilje aktivnosti |
| **Opis aktivnosti** | **TO-DO** opišite, kaj boste v okviru aktivnosti izvajali |
| **Morebitne odvisnosti in omejitve** | **TO-DO** Katere aktivnosti morajo biti zaključene, preden se lahko lotite obravnavane aktivnosti? Je obravnavana aktivnost na kritični poti? |
| **Pričakovani rezultati aktivnosti** | **TO-DO** navedite konretne izdelke aktivnosti |


### 3.3 Seznam izdelkov

V obliki zbirne table prikažite izdelke vseh aktivnosti, kjer lahko uporabite naslednjo tabelo:

| Oznaka izdelka | Ime izdelka | Datum izdaje | 
| :--- | :---------------------------- | :--- |
| **TO-DO** | **TO-DO** | **TO-DO** |
| ...       | ...       | ...       |


### 3.4 Časovni potek projekta - Ganttov diagram

**TO-DO**

* Obdobja izvajanja posameznih aktivnosti prikažite v obliki **Ganttovega diagrama**. 
* Za izdelavo Ganttovih diagramov obstajajo številna spletna orodja. 
* Za izdelavo Ganttovega diagrama si lahko izberete poljubno orodje, v poročilo vključite končno **sliko diagrama, ki naj bo jasno berljiva**.

![Ganttov diagram](../img/ganttov-diagram.png)


### 3.5 Odvisnosti med aktivnosti - Graf PERT

**TO-DO**

* Odvisnosti med aktivnostmi prikažite v obliki **grafa PERT**. 
* Na grafu naj bo jasno označena kritična pot. 
* Tudi za tovrstne diagrame obstaja več spletnih orodij. 
* Za izdelavo grafa PERT si izberete poljubno orodje, v poročilo vključite končno **sliko diagrama, ki naj bo jasno berljiva**.

![Graf PERT](../img/graf-pert.png)



## 4. Obvladovanje tveganj

### 4.1 Identifikacija in analiza tveganj

**TO-DO**

Opredelite možna tveganja in za vsakega navedite sledeče:

* naziv tveganja (npr. izpad spletnega strežnika),
* kratek opis tveganja,
* tip tveganja (tehnološko tveganje, organizacijsko tveganje, tveganje povezano z ljudmi, tveganje povezana z zahtevami, tveganje povezano z ocenjevanjem, tveganje povezana z orodji),
* verjetnost nastopa tveganja (zelo visoko, visoko, zmerno, nizko, zelo nizko),
* posledice nastopa tveganja (usodne, resne, dopustne, neznatne).

Identifikacijo in analizo tveganj prikažite z uporabo naslednje tabele:

| Naziv tveganja | Opis tveganja | Tip tveganja | Verjetnost nastopa tveganja | Posledice nastopa tveganja |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
| **TO-DO**      | **TO-DO**     | **TO-DO**    | **TO-DO**                   | **TO-DO**                  |
| ...            | ...           | ...          | ...                         | ...                        |


### 4.2 Načrtovanje tveganj

**TO-DO**

Pri načrtovanju tveganj si postavljamo **kaj-če vprašanja**, ki upoštevajo posamezna tveganja, kombinacijo tveganj in zunanje faktorje, ki lahko vplivajo na tveganja. Za vsako tveganje navedite:

* naziv tveganja in
* načrt za obvladovanje tega tveganja, kjer opišemo strategijo za obvladovanje tega tveganja (strategija izogibanja, strategija zmanjševanja, krizni načrt).

Načrt za obvladovanje tveganj prikažite z uporabo naslednje tabele:

| Tveganje  | Strategija |
| :-------- | :--------- |
| **TO-DO** | **TO-DO**  |
| ...       | ...        |



## 5. Upravljanje projekta

**TO-DO**

* Opišite organizacijo dela na projektu.



## 6. Predstavitev skupine

**TO-DO**

* Na kratko predstavite vsakega člana skupine (en odstavek za vsakega),
    * kaj bo delal na projektu, 
    * s kom bo pri tem sodeloval in 
    * zakaj je bil izbran za delo na dodeljenih aktivnostih.



## 7. Finančni načrt - COCOMO II ocena

**TO-DO**

* Izdelajte finančni načrt projekta po metodi COCOMO II. 
* Za vsako aktivnost navedite sledeče:
    * obseg v človek-mesecih,
    * predvidene stroške dela,
    * predvidene stroške investicij,
    * predvidene potne stroške in
    * druge predvidene posredne stroške.
* Seštejte predvidene stroške po posameznih aktivnostih in nato izračunajte še skupno vsoto vseh stroškov. 
* Tudi za ocenjevanje stroškov po metodi COCOMO II je na voljo več spletnih orodij. 
* Uporabite lahko poljubnega, v poročilo vključite končno **sliko izračuna, ki naj bo jasno berljiva**.

![COCOMO II ocena](../img/cocomo-ii-ocena.png)



## Reference

**TO-DO**

* Navedite vire, ki ste jih uporabljali pri sestavi projektnega plana.
* Primer navedbe vira [1].

[1]: R. H. Thayer, E. Yourdon, **Software Engineering Project**, IEEE Computer Society, Los Alamitos, 2001.